package ilonaszafner.quiz.controller;


import ilonaszafner.quiz.dao.QuestionDaoImplementation;
import ilonaszafner.quiz.dao.ScoreDaoImplementation;
import ilonaszafner.quiz.model.Question;
import ilonaszafner.quiz.model.QuestionDataBase;
import ilonaszafner.quiz.model.QuestionDataBaseHistory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@Controller
public class QuizController {

    List<Question> question = new QuestionDataBase().getQuestionDabaBase();
    List<Question> questionHistory = new QuestionDataBaseHistory().getQuestionDabaBaseHistory();

    Random random = new Random();
    int number;


    @GetMapping("/quiz")
    public String startPage() {
        return "hello";
    }


    @GetMapping("/quiz/biology")
    public String biologyPage(ModelMap modelMap) {
        number = random.nextInt(question.size());
        modelMap.put("sentence", question.get(number).getSentence());
        return "biology";
    }

    @GetMapping("/quiz/result")
    public String result(@RequestParam Boolean option, ModelMap modelMap) {
        if (option == true && question.get(number).getIsTrue().equals("true") ||
                option == false && question.get(number).getIsTrue().equals("false")
        ) {
            modelMap.put("resultTest", "Podałeś poprawną odpowiedź!");
        } else {
            modelMap.put("resultTest", "Podałeś NIEpoprawną odpowiedź");
        }
        return "result";
    }


    @GetMapping("/quiz/history")
    public String historyPage(ModelMap modelMap) {
        number = random.nextInt(questionHistory.size());
        modelMap.put("sentence", questionHistory.get(number).getSentence());
        return "biology";
    }

    @GetMapping("/quiz/results")
    public String resultHistory(@RequestParam Boolean option, ModelMap modelMap) {
        if (option == true && questionHistory.get(number).getIsTrue().equals("true") ||
                option == false && questionHistory.get(number).getIsTrue().equals("false")
        ) {
            modelMap.put("resultTest", "Podałeś poprawną odpowiedź!");
        } else {
            modelMap.put("resultTest", "Podałeś NIEpoprawną odpowiedź");
        }
        return "results";
    }


}
