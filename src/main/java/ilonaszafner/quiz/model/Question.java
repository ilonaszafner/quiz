package ilonaszafner.quiz.model;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class Question  {

    private int id;
    private String sentence;
    private String isTrue;

    public Question() {
    }

    public Question(int id, String sentence, String isTrue) {
        this.id = id;
        this.sentence = sentence;
        this.isTrue = isTrue;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSentence() {
        return sentence;
    }

    public void setSentence(String sentence) {
        this.sentence = sentence;
    }

    public String getIsTrue() {
        return isTrue;
    }

    public void setIsTrue(String isTrue) {
        this.isTrue = isTrue;
    }


    @Override
    public String toString() {
        return "Question{" +
                "id=" + id +
                ", sentence='" + sentence + '\'' +
                ", isTrue='" + isTrue + '\'' +
                '}';
    }
}
