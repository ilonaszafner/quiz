package ilonaszafner.quiz.model;

import java.util.ArrayList;
import java.util.List;

public class QuestionDataBase {

    public List<Question> getQuestionDabaBase  () {
        List<Question> allQuestionList = new ArrayList<>();
        allQuestionList.add(new Question(1, "Czy APOPTOZA to programowana śmierć komórki?", "true"));
        allQuestionList.add(new Question(2, "Czy Cytoplazma stwarza odpowiednie środowisko do transportu substancji między organellami, a błoną komórkową?", "true"));
        allQuestionList.add(new Question(3, "Czy Wodniczki występują w komórkach zwierząt?", "false"));
        allQuestionList.add(new Question(4, "Czy Mitochondria występują w komórkach zwierzęcych?", "true"));
        allQuestionList.add(new Question(5, "Czy Choroplasty występują w komórkach zwierzęcych?", "false"));
        allQuestionList.add(new Question(6, "Czy w jądrze komórkowym znajduje się jąderko?", "true"));
        allQuestionList.add(new Question(7, "Czy człowiek posiada 10 par żeber?", "false"));
        allQuestionList.add(new Question(8, "Czy u człowieka, kość promieniowa znajduje się w nodze?", "false"));
        allQuestionList.add(new Question(9, "Czy u człowieka, kość strzałkowa znajduje się w ręce?", "false"));
        allQuestionList.add(new Question(10, "Czy żołądek przeżuwaczy składa się z 4 komór?", "true"));
        allQuestionList.add(new Question(11, "Czy wątroba jest największym gruczołem organizmu ludzkiego?", "true"));
        allQuestionList.add(new Question(12, "Czy trzustka jest najmniejszym gruczołem organizmu ludzkiego?", "false"));
        allQuestionList.add(new Question(13, "Czy ściana tętnic i żył zbudowana jest z trzech warstw?", "true"));
        allQuestionList.add(new Question(14, "Czy erytrocyty to płytki krwi?", "false"));
        allQuestionList.add(new Question(15, "Czy trombocyty to płytki krwi?", "true"));
        allQuestionList.add(new Question(16, "Czy leukocyty to krwinki białe?", "true"));
        allQuestionList.add(new Question(17, "Czy ubytek krwi może zostać zatamowany przez retrakcję skrzepu?", "true"));
        allQuestionList.add(new Question(18, "Czy w budowie ucha człowieka występuje młoteczek i kowadełko?", "true"));
        allQuestionList.add(new Question(19, "Czy A. Fleming - szkocki lekarz i mikrobiolog, wykazał, że DNA jest nośnikiem informacji genetycznej?", "false"));
        allQuestionList.add(new Question(20, "Czy K. Funk - polski biochemik, jako pierwszy odkrył witaminy oraz uważał ich brak za przyczynę chorób?", "true"));
        allQuestionList.add(new Question(21, "Współżycie dwóch organizmów może być korzystne tylko dla jednego z nich, natomiast dla drugiego zupełnie obojętne. Taki układ nazywamy mutualizmem.", "false"));
        allQuestionList.add(new Question(22, "Współżycie dwóch organizmów może być korzystne dla dwojga z nich. Taki układ nazywamy symbiozą.", "true"));
        allQuestionList.add(new Question(23, "Kwas nukleinowy to materiał energetyczny i budulcowy, zaś jego podstawową odmianą jest glukoza.", "false"));
        allQuestionList.add(new Question(24, "Kwas nukleinowy to materiał energetyczny i budulcowy, zaś jego podstawową odmianą jest glukoza.", "false"));
        allQuestionList.add(new Question(25, "Chorobą wieńcową serca nazywamy nadciśnieniem tętniczym.", "false"));
        allQuestionList.add(new Question(26, "Gruczoły dokrewne produkują enzymy trawienne.", "false"));
        allQuestionList.add(new Question(27, "Niedobór insuliny we krwi powoduje wole.", "false"));
        allQuestionList.add(new Question(28, "Ślepotą na barwy nazywamy daltonizm.", "true"));
        allQuestionList.add(new Question(29, "Pierwszym odcinkiem jelita cienkiego to wyrostek robaczkowy.", "false"));
        allQuestionList.add(new Question(30, "Głównym produktem fotosyntezy jest cukier prosty.", "true"));
        allQuestionList.add(new Question(31, "W procesach anabolicznych energia jest pobierana.", "true"));
        allQuestionList.add(new Question(32, "Składnikiem krwi odpowiadającym za krzepnięcie to trombocyty.", "true"));
        allQuestionList.add(new Question(33, "Przedstawicielami gromady składającej skrzek są gady.", "false"));
        allQuestionList.add(new Question(34, "Zespół nabytego upośledzenia odporności to AIDS.", "true"));
        allQuestionList.add(new Question(35, "Zespołem wszystkich genów w komórce danego osobnika jest genotyp.", "true"));
        allQuestionList.add(new Question(36, "Składnikiem powodującym powstawanie kwaśnych opadów jest sadza.", "false"));
        allQuestionList.add(new Question(37, "Ściana komórkowa i mitochondria występują w komórce zwierzęcej.", "false"));
        allQuestionList.add(new Question(38, "Nie u wszystkich organizmów występuje chlorofil.", "true"));
        allQuestionList.add(new Question(39, "Fluor jest pierwiastkiem, który zapobiega próchnicy.", "true"));
        allQuestionList.add(new Question(40, "Składniki odżywcze i tlen są dostaczane do płodu za pośrednictwem błon płodowych.", "false"));
        allQuestionList.add(new Question(41, "Ptaki są zwierzętami wyłącznie latającymi?", "false"));
        allQuestionList.add(new Question(42, "Z mięśni gładkich zbudowane są ściany naczyń krwionośnych i jelit?", "true"));
        allQuestionList.add(new Question(43, "Chorobami bakteryjnymi są koklusz i błonica.", "true"));
        allQuestionList.add(new Question(44, "Anemia jest niedoborem żelaza w organiźmie.", "true"));
        allQuestionList.add(new Question(45, "Krzywica jest głównie spowodowana niedoborem witaminy D.", "true"));
        allQuestionList.add(new Question(46, "Pierwiastki chemiczne, które są niezbędne dla żywych organizmów i potrzebne w dużych ilościach, to mikroskładniki.", "false"));
        allQuestionList.add(new Question(47, "Organizmami, dla których miejscem bytowania i zdobywania pokarmu są inne organizmy żywe nazywamy pasożytami.", "true"));
        allQuestionList.add(new Question(48, "Produktem ubocznym fotosyntezy jest tlen.", "true"));
        allQuestionList.add(new Question(49, "Zębami do kruszenia i żucia pokarmu są zęby przedtrzonowe.", "true"));
        allQuestionList.add(new Question(50, "Język jest w stanie wykryć cztery podstawowe smaki. Smak słodki wykrywany jest na czubku języka.", "true"));
        allQuestionList.add(new Question(51, "Zębami do kruszenia i żucia pokarmu są zęby przedtrzonowe.", "true"));
        return allQuestionList;
    }

}
