package ilonaszafner.quiz.model;

import java.util.ArrayList;
import java.util.List;

public class QuestionDataBaseHistory {

    public List<Question> getQuestionDabaBaseHistory  () {
        List<Question> allQuestionListHistory = new ArrayList<>();
        allQuestionListHistory.add(new Question(1, "Czy wojna polsko-czeska trwała w lalatach 1345–1348", "true"));
        allQuestionListHistory.add(new Question(2, "Czy I Wojna Światowa przed II Wojną Światową była nazywana Wielką Wojną?", "true"));
        allQuestionListHistory.add(new Question(3, "Czy Wojny napoleońskie były serią konfliktów zbrojnych pomiędzy Francją i państwami z nią sprzymierzonymi, a zmieniającą się koalicją innych państw Europy", "false"));
        allQuestionListHistory.add(new Question(4, "Czy Imperium Osmańskie było państwo tureckie na Dalekim Wschodzie?", "false"));
        allQuestionListHistory.add(new Question(5, "Termin sułtan powstał w czasach kalifatu bagdadzkiego?", "true"));
        allQuestionListHistory.add(new Question(6, "Czy Napoleon Bonaparte objął stanowisko Drugiego Konsula?", "true"));
        return allQuestionListHistory;
    }

}
